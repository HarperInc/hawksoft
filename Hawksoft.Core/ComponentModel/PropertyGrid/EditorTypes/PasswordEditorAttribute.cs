﻿using System;
using System.Globalization;
using System.Reflection;

namespace Hawksoft.ComponentModel
{
    /// <summary>
    /// Indicates that the target property should use a "Password" editor.
    /// </summary>
    /// <seealso cref="Hawksoft.ComponentModel.CustomEditorAttribute" />
    public partial class PasswordEditorAttribute : CustomEditorAttribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PasswordEditorAttribute"/> class.
        /// </summary>
        public PasswordEditorAttribute()
            : base("Password")
        {
        }
    }
}