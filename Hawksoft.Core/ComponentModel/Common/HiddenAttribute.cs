﻿
namespace Hawksoft.ComponentModel
{
    /// <summary>
    /// Sets a column as initially hidden
    /// </summary>
    /// <seealso cref="Hawksoft.ComponentModel.VisibleAttribute" />
    public class HiddenAttribute : VisibleAttribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HiddenAttribute"/> class.
        /// </summary>
        public HiddenAttribute()
            : base(false)
        {
        }
    }
}