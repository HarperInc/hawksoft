﻿using System;

namespace Hawksoft.Abstractions
{
    /// <summary>
    /// Date/time abstraction for unit testing.
    /// </summary>
    public interface IDateTimeProvider
    {
        /// <summary>
        /// Gets the now.
        /// </summary>
        /// <value>
        /// The now.
        /// </value>
        DateTime Now { get; }
    }
}