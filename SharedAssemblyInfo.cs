﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyCompany("Harper Capital")]
[assembly: AssemblyProduct("Hawksoft Platform")]
[assembly: AssemblyCopyright("Copyright © Scott Weeden")]
[assembly: AssemblyVersion("0.8.1")]
[assembly: AssemblyFileVersion("0.8.1")]
